(function ($) {
 Drupal.behaviors.exampleModule = {
   attach: function (context, settings) {
        "use strict";
    (new WOW).init(),
    $(".testimonial_carosel").length&&$(".testimonial_carosel").owlCarousel({
        loop:!0,
        items:1,
        autoplay:!1
    }),
    $(".partners").length && $(".partners").owlCarousel({
        loop: !0,
        items: 5,
        margin: 110,
        autoplay: !0,
        response: !0,
        responsive: {
            300: {
                items: 1,
                margin: 0
            },
            500: {
                items: 3
            },
            700: {
                items: 3
            },
            800: {
                items: 4,
                margin: 40
            },
            1e3: {
                items: 5
            }
        }
    }),
    $(".portfolio_item, .portfolio_2 .portfolio_filter ul li").length&&(
        $(".portfolio_item").imagesLoaded(function(){
            $(".portfolio_item").isotope({
                itemSelector:".single_facilities",
                layoutMode:"masonry",percentPosition:!0,
                masonry:{
                    columnWidth:".grid-sizer, .grid-sizer-2"
                }
            })
        }),
        $(".portfolio_2").imagesLoaded(function(){
            $(".portfolio_2").isotope({
                itemSelector:".single_facilities",
                layoutMode:"fitRows"
            })
        }),
        $(".portfolio_filter ul li").on("click",function(){
            $(".portfolio_filter ul li").removeClass("active"),
            $(this).addClass("active");
            var e=$(this).attr("data-filter");
            return $(".portfolio_item, .portfolio_2").isotope({
                filter:e,animationOptions:{
                    duration:450,easing:"linear"
                    ,queue:!1
                }
            }),!1
    })),
    $(".our_skill_inner").length&&$(".our_skill_inner").each(function(){
        $(this).waypoint(function(){
            $(".progress-bar").each(function(e){
                $(this).css("width",$(this).attr("aria-valuenow")+"%")
            })
        },
        {triggerOnce:!0,offset:"bottom-in-view"})
    }),
    $(".preloader").length&&$(window).load(function(){
        $(".preloader").delay(500).fadeOut("slow"),
        $("body").delay(500).css({overflow:"visible"})
    });
    $("body").scrollspy({
        target: "#mainNav",
        offset: 56
    });
        $(".portfolio-modal").on("show.bs.modal", function(e) {
        $(".navbar").addClass("d-none")
    }); $(".portfolio-modal").on("hidden.bs.modal", function(e) {
        $(".navbar").removeClass("d-none")
    });
    $(".icons").click(function () {
        $(".dropdown-menu ").css("display","none");
          $(".icon").css("display","block");
             $(".icons").css("display","none");
          });
    $(".icon").click(function () {
        $(this).parent().children('.icons').css("display","block");
        $(this).parent().children('.dropdown-menu').css("display","block");
        $(this).parent().children('.icon').css("display","none");
    });
    $(window).on('load', function(){
            $(".featured_works .modal").each(function(){
                $(this).clone().appendTo("body");
                $(this).remove();
            });
            $('ul.gallery-temp-filters li').each(function(){
                $(this).clone().appendTo(".portfolio_filter ul");
                $(this).remove();
            });
    });
    $('.node__submitted').remove();
    $('.blog_content h4 a').addClass('c-b font_14_roboto');
    
}}})(jQuery, Drupal);
